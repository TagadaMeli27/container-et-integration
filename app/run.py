#! /usr/bin/env python3

from flask import Flask, render_template

app = Flask(__name__)

import requests

API_URL = "http://todos-api:8082/"

@app.route("/")
def home():
    return "<h1>Bienvenue il y a au moins ça qui marche !</h1>"

@app.route("/<user>")
def todo(user):
    todos = requests.get(API_URL+user).json()["todos"]

    return render_template(
        "user.html",
        user = user,
        todos = todos
    )

app.run(host="0.0.0.0", port=7777)