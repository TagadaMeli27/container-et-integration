# Container et Intégration

Cours de container et intégration (docker)

# Lancement

## Installation db

* mkdir mongo
* mkdir db (dans mongo)

## Build des images

* *docker build -t tagadameli27/todos-app app/* (dans le dossier parent)
* *docker build -t tagadameli27/todos-api api/* (dans le dossier parent)

## Lancement

* *docker run -d -p 4000:27017 -v /workspace/container-et-integration/mongo/db:/data/db --name todos-mongo mvertes/alpine-mongo*
* *docker run -d -p 4001:8082 -v /workspace/container-et-integration/api:/api --name todos-api tagadameli27/todos-api:v1*
* *docker run -d -p 4002:7777 -v /workspace/container-et-integration/app:/app --name todos-app tagadameli27/todos-app*

**Lien api / BDD**

* *docker run --network todos-net -d -v /workspace/container-et-integration/mongo/db:/data/db --name todos-mongo mvertes/alpine-mongo* (retirer le container todos-mondo pour le faire)

# Images

## Chercher :

docker pull nom_image

## Lister toutes :

docker image ls

# Containers

## Lister tous :

docker ps -a

## Lister ceux en fonctionnement :

docker ps

## Retirer un container :

docker rm -f nom (-f s'ils tournent)

# Network

## Créer un network :

docker network create nom_network